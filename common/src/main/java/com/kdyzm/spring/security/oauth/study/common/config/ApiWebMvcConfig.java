package com.kdyzm.spring.security.oauth.study.common.config;


import com.kdyzm.spring.security.oauth.study.common.AccessLogInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author kdyzm
 */
@Configuration
@ComponentScan(basePackages = {"com.kdyzm.spring.security.oauth.study.common"})
public class ApiWebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private AccessLogInterceptor accessLogInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(accessLogInterceptor).addPathPatterns("/**");
    }
}
